#include "spiLCD.h"

void spilcd_write_byte(uint8_t data, uint8_t reg){
    uint8_t pin_buffer = 0;
    
    /*First we need to check if the busy flag is still set from a previous instruction*/
    mcp23_write_reg_once(EXP_SPI_ADDR, EXP_ADDR_IODIR, (1<<LCD_PIN_D4) | (1<<LCD_PIN_D5) | (1<<LCD_PIN_D6) | (1<<LCD_PIN_D7)); //setup data lines as inputs to read busy flag
    mcp23_start(EXP_SPI_ADDR, EXP_ADDR_GPIO, MCP_WRITE); //start a new transmission to read the busy flag
    
    pin_buffer = (1<<LCD_PIN_RW) | (1<<LCD_PIN_ENABLE) | (lcd_enable_backlight<<LCD_PIN_BL) | (LCD_IREG<<LCD_PIN_RS); //set RW to high to read from the LCD, REG-SEL low to read from instruction register
    mcp23_write_reg(pin_buffer);
    
    pin_buffer &= ~(1<<LCD_PIN_ENABLE); //write enable low
    mcp23_write_reg(pin_buffer);
    mcp23_stop();
    
    mcp23_start(EXP_SPI_ADDR, EXP_ADDR_GPIO, MCP_READ);
    for(uint16_t i = 0; i<LCD_MAX_BF_CYCLES; i++){ //for loop to prevent getting stuck in an infinite wait loop in case the display is broken
        if(!(mcp23_read_reg() & (1<<LCD_PIN_D7))){ //read the busy flag
            break;
        }
    }
    mcp23_stop();   
    
    /*Busy flag is not set anymore, we can now shift out actual data*/
    mcp23_write_reg_once(EXP_SPI_ADDR, EXP_ADDR_IODIR, 0x00); //make sure all pins are outputs
    
    mcp23_start(EXP_SPI_ADDR, EXP_ADDR_GPIO, MCP_WRITE);
    
    pin_buffer = ((data & 0b11110000) >> 1) | (lcd_enable_backlight<<LCD_PIN_BL) | (1<<LCD_PIN_ENABLE) | ((reg & 1) << LCD_PIN_RS); //write the first 4 bits and keep enable high
    mcp23_write_reg(pin_buffer); 
    
    pin_buffer &= ~(1<<LCD_PIN_ENABLE); //write enable low
    mcp23_write_reg(pin_buffer);
//    delay_us(1); //waint 1 microsecond
    
    pin_buffer |= (1<<LCD_PIN_ENABLE); //write enable high
    mcp23_write_reg(pin_buffer);
    
    pin_buffer = ((data & 0b00001111) << 3) | (lcd_enable_backlight<<LCD_PIN_BL) | ((reg & 1) << LCD_PIN_RS) | (1<<LCD_PIN_ENABLE); //write the last 4 bits
    mcp23_write_reg(pin_buffer); 
    
    pin_buffer &= ~(1<<LCD_PIN_ENABLE); //write enable low
    mcp23_write_reg(pin_buffer);
//    delay_us(1); //wait 1 microsecond
    
    pin_buffer |= (1<<LCD_PIN_ENABLE); //write enable high
    mcp23_write_reg(pin_buffer);
    
    mcp23_stop();

}

void spilcd_init(uint8_t display_on, uint8_t cursor_on, uint8_t blink, uint8_t move_right, uint8_t shift){
    /*First, we need to initialize the port expander*/
    mcp23_write_reg_once(EXP_SPI_ADDR, EXP_ADDR_IOCON, 1<<5); //write SEQOP to 1 to disable auto address incrementing
    mcp23_write_reg_once(EXP_SPI_ADDR, EXP_ADDR_IODIR, 0x00); //configure all pins as outputs
    
    /*Now we can initialize the LCD itself*/
    spilcd_write_byte(LCD_INST_FNSET, LCD_IREG); //set function: 4 bits, 2 lines, 5x8 dots font
    spilcd_write_byte(LCD_INST_ENTRYMODE | ((move_right & 1)<<1) | (shift & 1), LCD_IREG); //setup cursor and display shift
    spilcd_write_byte(LCD_INST_ON_OFF | ((display_on & 1)<<1) | ((cursor_on & 1)<<1) | (blink & 1), LCD_IREG); //enable display, cursor and cursor blinking
}

void spilcd_clear(){
    spilcd_write_byte(LCD_INST_CLEAR, LCD_IREG);
}

void spilcd_home(){
    spilcd_write_byte(LCD_INST_HOME, LCD_IREG);
}

void spilcd_set_entrymode(uint8_t move_right, uint8_t shift){
    spilcd_write_byte(LCD_INST_ENTRYMODE | ((move_right & 1)<<1) | (shift & 1), LCD_IREG);
}

void spilcd_set_displaymode(uint8_t display_on, uint8_t cursor_on, uint8_t blink){
    spilcd_write_byte(LCD_INST_ON_OFF | ((display_on & 1)<<1) | ((cursor_on & 1)<<1) | (blink & 1), LCD_IREG);
}

void spilcd_shift(uint8_t shift_display, uint8_t shift_right){
    spilcd_write_byte(LCD_INST_SHIFT | ((shift_display & 1)<<3) | ((shift_right & 1)<<2), LCD_IREG);
}

void spilcd_set_cgramaddr(uint8_t addr){
    spilcd_write_byte(LCD_INST_SET_CGADDR | (addr & 0b00111111), LCD_IREG);
}

void spilcd_set_ddramaddr(uint8_t addr){
    spilcd_write_byte(LCD_INST_SET_DDADDR | (addr & 0b01111111), LCD_IREG);
}

void spilcd_setpos(uint8_t xpos, uint8_t ypos){
    spilcd_set_ddramaddr(xpos + ypos * 0x40);
}

void spilcd_putc(char data){
    spilcd_write_byte((uint8_t) data, LCD_DREG);
}

void spilcd_puts(char *data){
    for(uint8_t i = 0; i<strlen(data); i++){
        spilcd_write_byte(data[i], LCD_DREG);
    }
}

void spilcd_puts_P(const char *data){
    char curr_byte;
    while((curr_byte = pgm_read_byte(data))){
        spilcd_write_byte(curr_byte, LCD_DREG);
    }
}