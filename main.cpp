#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define __AVR_ATmega328__

#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include "spiLCD.h"
#include "spiLCD.c"

void delay_us(uint16_t us){
    for(; us > 0; us--){
         _delay_us(1);
    }
}

void delay_ms(uint16_t ms){
    for(; ms > 0; ms--){
         _delay_ms(1);
    }
}

int main(void) {
    
    DDRC = (1<<PC3);
    PORTC = (1<<PC3);
    spi_master_init(SPI_DORD_LSB, SPI_CLK_PHA1, SPI_CLK_PHA1, SPI_PRESC_16);
    spi_transmit_byte(0b11110000);
    //spilcd_init(1, 0, 0, 1, 0);
    spilcd_clear();
    spilcd_home();
    
    while (1) {
        spilcd_puts("Ha");
//        PORTC &= ~(1<<PC3);
//        spi_transmit_byte(0b11100000);
//        PORTC |= (1<<PC3);
        delay_ms(1000);
    }
    
}
