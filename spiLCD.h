#ifndef SPILCD_H
#define SPILCD_H

#include <avr/io.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "mcp23s08/megaspi/megaSPI.h"
#include "mcp23s08/mcp23s08.h"
#include "mcp23s08/mcp23s08.c"

#define EXP_SPI_ADDR 0b01000000 //the SPI address of the port expander IC 

#define LCD_PIN_ENABLE  0
#define LCD_PIN_RW      1
#define LCD_PIN_RS      2
#define LCD_PIN_D4      3
#define LCD_PIN_D5      4
#define LCD_PIN_D6      5
#define LCD_PIN_D7      6
#define LCD_PIN_BL      7

#define LCD_MAX_BF_CYCLES 50000 //maximum amount of cycles to wait for the BF to dissappear (prevents infinite loops)
#define LCD_IREG 0
#define LCD_DREG 1
#define LCD_INST_CLEAR      0b00000001
#define LCD_INST_HOME       0b00000010
#define LCD_INST_ENTRYMODE  0b00000100
#define LCD_INST_ON_OFF     0b00001000
#define LCD_INST_SHIFT      0b00010000
#define LCD_INST_FNSET      0b00100000
#define LCD_INST_SET_CGADDR 0b01000000
#define LCD_INST_SET_DDADDR 0b10000000

uint8_t lcd_enable_backlight;

/*Writes a byte to the LCD in two 4-bit operations*/
extern void spilcd_write_byte(uint8_t data, uint8_t reg);

/*Initializes the LCD and the port expander IC*/
extern void spilcd_init(uint8_t display_on, uint8_t cursor_on, uint8_t blink, uint8_t move_right, uint8_t shift);

/*clears the LCD*/
extern void spilcd_clear(void);

/*sets the cursor position to 0 0*/
extern void spilcd_home(void);

/*Sets the display entry mode (cursor move direction and optional display shift on write)*/
extern void spilcd_set_entrymode(uint8_t move_right, uint8_t shift);

/*Sets the display mode (display on/off, cursor on/off, cursor blinking on/off)*/
extern void spilcd_set_displaymode(uint8_t display_on, uint8_t cursor_on, uint8_t blink);

/*Shifts the whole display right or left*/
extern void spilcd_shift(uint8_t shift_display, uint8_t shift_right);

/*Sets the current CG-Ram address*/
extern void spilcd_set_cgramaddr(uint8_t addr);

/*Sets the current DD-Ram address*/
extern void spilcd_set_ddramaddr(uint8_t addr);

/*Sets the cursor X and Y position (0,0 = top left corner)*/
extern void spilcd_setpos(uint8_t xpos, uint8_t ypos);

/*Writes a single character to the LCD at the current cursor position*/
extern void spilcd_putc(char data);

/*Writes a string to the LCD starting at the current cursor position*/
extern void spilcd_puts(char *data);

/*Writes a string from Progmem to the LCD, starting at the current cursor position*/
extern void spilcd_puts_P(const char *data);

#endif /* SPILCD_H */

